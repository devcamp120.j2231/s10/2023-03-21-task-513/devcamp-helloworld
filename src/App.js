const gDevcampReact = {
  title: 'Chào mừng đến với Devcamp React',
  image: 'https://codelearn.io/Upload/Blog/react-js-co-ban-phan-1-63738082145.3856.jpg',
  benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
  studyingStudents: 20,
  totalStudents: 100,
  countPercentStudyingStudent() {
    return this.studyingStudents / this.totalStudents * 100;
  }
}

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} width={500} alt="Title"/>
      <p>Tỷ lệ sinh viên đang theo học: {gDevcampReact.countPercentStudyingStudent()} %</p>
      <p>{ gDevcampReact.countPercentStudyingStudent() > 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít" }</p>
      <ul>
        {gDevcampReact.benefits.map((element, index) => {
          return <li key={index}>{element}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
